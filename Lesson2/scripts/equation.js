/**
 * Created by Rostislav on 20-Nov-16.
 */
"use strict";

function myFunction() {
    var a = document.getElementById("a").value;
    if (a == '' || a == 0)a = 1;
    var b = document.getElementById("b").value;
    if (b == '')b = 1;
    var c = document.getElementById("c").value;
    if (c == '')c = 0;
    var r = document.getElementById("r").value;
    if (r == '')r = 0;
    var X = [];
    X = arg(a, b, c);
    alert('X1=' + X[0] + ', X2=' + X[1]);

}

function arg(a, b, c) {
    var a = document.getElementById("a").value;
    if (a == '' || a == 0)a = 1;
    var b = document.getElementById("b").value;
    if (b == '')b = 1;
    var c = document.getElementById("c").value;
    if (c == '')c = 0;
    var r = document.getElementById("r").value;
    if (r == '')r = 0;

    var rez;
    if (b == 0) {
        rez = a + 'X<sup>2</sup>+' + c + '=' + r;
        if (c == 0) {
            rez = a + 'X<sup>2</sup>=' + r;
            if (a == 1)rez = 'X<sup>2</sup>=' + r;
        }
        if (c < 0) {
            rez = a + 'X<sup>2</sup>' + c + '=' + r;
            if (a == 1)rez = 'X<sup>2</sup>' + c + '=' + r;
        }
    }
    else if (b < 0) {
        rez = a + 'X<sup>2</sup>' + b + 'X+' + c + '=' + r;
        if (c == 0) {
            rez = a + 'X<sup>2</sup>' + b + 'X=' + r;
            if (a == 1)rez = 'X<sup>2</sup>' + b + 'X=' + r;
        }
        if (c < 0) {
            rez = a + 'X<sup>2</sup>' + b + 'X' + c + '=' + r;
            if (a == 1)rez = 'X<sup>2</sup>' + b + 'X' + c + '=' + r;
        }
    }
    else {
        if (c < 0) {
            rez = a + 'X<sup>2</sup>+' + b + 'X' + c + '=' + r;
            if (a == 1)rez = 'X<sup>2</sup>+' + b + 'X' + c + '=' + r;
        }
        else if (c == 0) {
            rez = a + 'X<sup>2</sup>+' + b + 'X=' + r;
            if (a == 1)rez = 'X<sup>2</sup>+' + b + 'X=' + r;
        }
        else {
            if (a != 1)rez = a + 'X<sup>2</sup>+' + b + 'X+' + c + '=' + r;
            else rez = 'X<sup>2</sup>+' + b + 'X+' + c + '=' + r;
        }
    }


    document.getElementById("ec").innerHTML = 'Equation:</br><font color="red">' + rez + '</font>';
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////REG EXSP//////////////////////////////////////
    var mystring = document.getElementsByTagName('html')[0].innerHTML;
    var regex = /([-0-9]+)*X<sup>2<\/sup>+([+0-9-]+)*X([+0-9-]+)*=([0-9])/;
    var match = regex.exec(mystring);
    if (match == null) {
        regex = /([-0-9]+)*X<sup>2<\/sup>+([+0-9-]+)*=([0-9])/;
        match = regex.exec(mystring);
        if (match[1] == undefined)match[1] = 1;
        if (match[2] == undefined)match[2] = 0;
        alert('a=' + match[1]+';b=0;c=' + match[2]+';r=' + match[3]);
    }


    else {
        if (match[1] == undefined)match[1] = 1;
        if (match[3] == undefined)match[3] = 0;
        alert('a=' + match[1]+';b=' + match[2]+';c=' + match[3]+';r=' + match[4]);
    }
    ////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////

    if (r) {
        c -= r;
        r = 0;
    }

    var x1, x2;
    var first, second;

    var delta = (b * b) - (4 * a * c);
    document.getElementById("ec").innerHTML += '</br>Δ:</br><font color="red">' + b + '<sup>2</sup>-4*' + a + '*' + c + '</font>';
    document.getElementById("ec").innerHTML += '</br>Solve the Δ:</br><font color="red">Δ=' + delta + '</font>';
    if (delta >= 0) {
        x1 = (((-b) - Math.sqrt(delta)) / (2 * a)).toString();
        x1 = x1.substring(0, 4);
        x2 = (((-b) + Math.sqrt(delta)) / (2 * a)).toString();
        x2 = x2.substring(0, 4);
    }
    else {
        delta = -delta;
        first = ((-b) / (2 * a)).toString();
        first = first.substring(0, 4);
        second = (Math.sqrt(delta) / (2 * a)).toString();
        second = second.substring(0, 4);

        x1 = first + '-' + second + 'i';
        x2 = first + '+' + second + 'i';

    }

    document.getElementById("x1").innerHTML = 'X1:</br>';
    document.getElementById("x1").innerHTML += '<font color="red">(' + (-b) + '-√' + delta + ')/(2*' + a + ')</font></br>';
    document.getElementById("x1").innerHTML += 'Solve X1:</br>';
    document.getElementById("x1").innerHTML += '<font color="red">X1=' + x1 + '</font>';
    document.getElementById("x2").innerHTML = 'X2:</br>';
    document.getElementById("x2").innerHTML += '<font color="red">(' + (-b) + '+√' + delta + ')/(2*' + a + ')</font></br>';
    document.getElementById("x2").innerHTML += 'Solve X2:</br>';
    document.getElementById("x2").innerHTML += '<font color="red">X2=' + x2 + '</font>';

    document.getElementById("canv").innerHTML = '</br>The graph of the function:</br>'
    document.getElementById("myCanvas").style.visibility = 'visible';
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var k1, k2, y;
    var y1 = [];
    var min, max;
    ctx.beginPath();
    ctx.moveTo(0, 0);
    for (k1 = -5; k1 < 6; k1++) {
        y = (a * (k1 * k1)) + (b * k1) + c;
        y1[k1 + 5] = y;
    }
    max = Math.max.apply(Math, y1);
    min = Math.min.apply(Math, y1);
    var dif = max - min;
    document.getElementById("table").style.visibility = 'visible';
    document.getElementById("interval").style.visibility = 'visible';
    document.getElementById("table").innerHTML = '<tr><td>X</td><td>Y</td></tr>';
    for (k1 = -5; k1 < 6; k1++) {


        y = (a * (k1 * k1)) + (b * k1) + c;
        document.getElementById("table").innerHTML += '<tr><td>' + k1 + '</td><td>' + y + '</td></tr>';
        y = y - min;
        y = dif / y;
        y = 150 / y;
        y = 150 - y;
        if (k1 == -5)ctx.moveTo(0, y);
        else {
            k2 = (5 + k1) * 30;
            ctx.lineTo(k2, y);
        }
    }

    ctx.stroke();
    var inte = max - min;
    inte = inte / 4;

    var inter = min;
    for (var l = 4; l >= 0; l--) {
        document.getElementById((l + 1).toString()).innerHTML = inter.toString();
        inter += inte;
    }

    return [x1, x2];

}


